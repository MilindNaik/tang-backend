﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TANGDataAccess2;

namespace TANG_Test.Models
{
    public class JoinPumpStockClass
    {
       public Pump Pumps { get; set; }
       public Stock Stocks { get; set; }
    }
}