﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TANGDataAccess2;
using TANG_Test.Models;

namespace TANG_Test.Controllers
{
    public class PumpsController : ApiController
    {
        public IHttpActionResult GetPumpData()
        {
            TANG_TestEntities entities = new TANG_TestEntities();
            List<Pump> lpumps = entities.Pumps.ToList();
            List<Stock> lstocks = entities.Stocks.ToList();
            var query = from p in lpumps
                        join s in lstocks on p.AppID equals s.AppID into table1
                        from s in table1.DefaultIfEmpty()
                        select new JoinPumpStockClass { Pumps = p, Stocks = s };
            return Ok(query);
        }

        public IHttpActionResult GetPump(string id)
        {
            using (TANG_TestEntities entities = new TANG_TestEntities())
            {
                Pump sPump = entities.Pumps.FirstOrDefault(p => p.AppID == id);
                Stock sStock = entities.Stocks.FirstOrDefault(s => s.AppID == id);
                var query = new JoinPumpStockClass { Pumps = sPump, Stocks = sStock };
                return Ok(query);
            }
        }
    }
}
