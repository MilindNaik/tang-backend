﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TANGDataAccess2;

namespace TANG_Test.Controllers
{
    public class ProjectPumpsController : ApiController
    {
        public IEnumerable<ProjectPump> Get()
        {
            using (TANG_TestEntities entities = new TANG_TestEntities())
            {
                return entities.ProjectPumps.ToList();
            }
        }

        public void POST([FromBody] ProjectPump projectPumpRecord)
        {
            using (TANG_TestEntities entities = new TANG_TestEntities())
            {
                entities.ProjectPumps.Add(projectPumpRecord);
                entities.SaveChanges();
            }
        }
    }
}
