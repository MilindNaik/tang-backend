﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TANGDataAccess2;
using TANG_Test.Models;
using System.Data.SqlClient;
using System.Data;

namespace TANG_Test.Controllers
{
    public class ProjectsController : ApiController
    {
        //public IEnumerable<Project> Get()
        //{
        //    using (TANG_TestEntities entities = new TANG_TestEntities())
        //    {
        //        return entities.Projects.ToList();
        //    }
        //}

        public HttpResponseMessage Get()
        {
            SqlConnection con;
            SqlDataAdapter adapter;
            DataSet ds = new DataSet();

            try
            {
                con = new SqlConnection("data source=.;initial catalog=TANG_Test;integrated security=True");
                String query = "SELECT * FROM Project";
                con.Open();
                adapter = new SqlDataAdapter(query, con);
                adapter.Fill(ds);
            }
            catch (Exception ex)
            {
                //con.Close();
                Console.WriteLine(ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, ds);

        }



        //public void POST([FromBody] Project projectRecord)
        //{
        //    using (TANG_TestEntities entities = new TANG_TestEntities())
        //    {
        //         entities.Projects.Add(projectRecord);
        //         entities.SaveChanges();       
        //    }
        //}

        public HttpResponseMessage POST([FromBody] Project projectRecord)
        {
            SqlConnection con;
            SqlDataAdapter adapter;
            SqlDataAdapter adapter1;
            SqlDataAdapter adapter2;
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            int PID=0 ;
            try
            {
                con = new SqlConnection("data source=.;initial catalog=TANG_Test;integrated security=True");
                String query = "INSERT INTO Project (ProjectName) VALUES('" + projectRecord.ProjectName +"')";
                con.Open();
                adapter = new SqlDataAdapter(query, con);
                String query1 = "SELECT TOP 1 PID FROM Project ORDER BY PID DESC";
                adapter1 = new SqlDataAdapter(query1, con);
                adapter.Fill(ds);
                adapter1.Fill(ds1);

                PID = (int)ds1.Tables[0].Rows[0]["PID"];

                String query2 = "INSERT INTO ProjectPumps (PID,AppID,Quantity) VALUES('"+PID+"','"+projectRecord.AppID+"','"+projectRecord.Quantity+"')";         
                adapter2 = new SqlDataAdapter(query2, con);
                adapter2.Fill(ds2);
            }
            catch (Exception ex)
            {
                //con.Close();
                Console.WriteLine(ex);
            }  
            return Request.CreateResponse(HttpStatusCode.OK, ds2);
        }






    }
}
